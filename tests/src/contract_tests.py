import atexit
import unittest
import requests

from pact import Consumer, Provider

pact = Consumer('Consumer').has_pact_with(Provider('Provider'), host_name="localhost", port=8080)
pact.start_service()
atexit.register(pact.stop_service)


class GetUserInfoContract(unittest.TestCase):

    def test_send_email_missing_recipient(self):
        result = requests.post('http://localhost:9753/send_email/', json={"message": "test",
                                                                          "subject": "test"})
        self.assertEqual(400, result.status_code)

    def test_send_email_missing_message(self):
        result = requests.post('http://localhost:9753/send_email/', json={"recipient": "test@test.cz",
                                                                          "subject": "test"})
        self.assertEqual(400, result.status_code)

    def test_send_email_missing_subject(self):
        result = requests.post('http://localhost:9753/send_email/', json={"recipient": "test@test.cz",
                                                                          "message": "test"})
        self.assertEqual(400, result.status_code)

    def test_send_email_missing_invalid_email_format(self):
        result = requests.post('http://localhost:9753/send_email/', json={"recipient": "test.cz",
                                                                          "message": "test",
                                                                          "subject": "test"})
        self.assertEqual(400, result.status_code)

    def test_send_email(self):
        result = requests.post('http://localhost:9753/send_email/', json={"recipient": "test@test.cz",
                                                                          "message": "test",
                                                                          "subject": "test"})
        self.assertEqual(result.status_code, 204)


if __name__ == '__main__':
    unittest.main()
