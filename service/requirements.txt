asgiref==3.2.7
certifi==2020.4.5.1
chardet==3.0.4
click==7.1.2
Django==3.0.6
flux==1.3.5
idna==2.9
Logbook==1.5.3
pact-python==0.21.0
psutil==5.7.0
pytz==2020.1
requests==2.23.0
six==1.14.0
sqlparse==0.3.1
urllib3==1.25.9
waiting==1.4.1
